<?php

namespace Drupal\Tests\codev_commerce\Functional;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: StandardTest.php
 * .
 */

/**
 * Class StandardTest.
 *
 * Test basic class for functionality tests.
 *
 * Simple test with the basic settings based on the standard profile and
 * template for further specific function test.
 *
 * @package      Drupal\Tests\codev_commerce\Functional
 *
 * @group        codev_commerce
 *
 * @noinspection PhpUnused
 */
class StandardTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Standard test.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testStandard() {
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
  }

}
