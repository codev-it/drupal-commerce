<?php
/** @noinspection PhpHierarchyChecksInspection */

namespace Drupal\Tests\codev_commerce\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\RequirementsPageTrait;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: FunctionalTestBase.php
 * .
 */

/**
 * Class FunctionalTestBase.
 *
 * Default functional test base settings.
 *
 * @package      Drupal\Tests\codev_commerce\Functional
 *
 * @group        codev_commerce
 *
 * @noinspection PhpUnused
 */
abstract class FunctionalTestBase extends BrowserTestBase {

  use SchemaCheckTestTrait;
  use RequirementsPageTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_commerce',
  ];

}
