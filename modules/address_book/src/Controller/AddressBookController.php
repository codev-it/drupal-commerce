<?php

namespace Drupal\codev_commerce_address_book\Controller;

use Drupal;
use Drupal\codev_commerce_address_book\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\commerce_order\Controller\AddressBookController as AddressBookControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\FocusFirstCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Render\Element;
use Drupal\Core\Theme\ThemeManager;
use Drupal\Core\Url;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\profile\Entity\ProfileTypeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: AddressBookController.php
 * .
 */

/**
 * Class AddressBookController.
 *
 * @package Drupal\codev_commerce_address_book\Controller
 */
class AddressBookController extends AddressBookControllerBase {

  /**
   * Address book ajax wrapper class.
   */
  const AJAX_WRAPPER_CLASS = 'address-book__wrapper';

  /**
   * Address book ajax inline add form wrapper class.
   */
  const AJAX_INLINE_ADD_FORM_CLASS = 'address-book__inline-add_form';

  /**
   * Address book ajax inline edit form wrapper class.
   */
  const AJAX_INLINE_EDIT_FORM_CLASS = 'address-book__inline-edit_form';

  /**
   * Address book ajax inline form wrapper class.
   */
  const AJAX_INLINE_ENTITY_WRAPPER_CLASS = 'address-book__profiles';

  /**
   * Address book ajax inline form wrapper class.
   */
  const AJAX_INLINE_ENTITY_CLASS = 'address-book__profile--';

  /**
   * @var \Drupal\Core\Theme\ThemeManager
   */
  private ThemeManager $themeHandler;

  /**
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  private ModuleHandler $moduleHandler;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): AddressBookController {
    $instance = parent::create($container);
    $instance->themeHandler = $container->get('theme.manager');
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function overviewPage(UserInterface $user): array {
    $build = parent::overviewPage($user);
    $settings = Settings::toArray();
    $handling = $settings['handling'] ?? '';

    if (Drupal::request()->isXmlHttpRequest()) {
      $build['status_messages'] = [
        '#type'   => 'status_messages',
        '#weight' => -99,
      ];
    }

    foreach (Element::children($build) as $type) {
      $profile_type = &$build[$type];

      if ($handling !== 'default') {
        $build['#prefix'] = sprintf('<div class="%s">', static::AJAX_WRAPPER_CLASS);
        $build['#suffix'] = '</div>';
      }

      if ($profile_type['add']) {
        switch ($handling) {
          case 'modal':
            Utils::appendDialogModalAttr($profile_type['add']['#attributes']);
            break;
          case 'inline':
            /** @var \Drupal\Core\Url $url */
            $url = $profile_type['add']['#url'];
            $profile_type['add']['#url'] = Url::fromRoute(
              'codev_commerce_address_book.address_book.add_form_ajax',
              $url->getRouteParameters());
            $profile_type['add']['#attributes']['class'][] = 'use-ajax';
            $profile_type['add']['#weight'] = -99;
            $profile_type['inline_form'] = [
              '#type'       => 'container',
              '#attributes' => ['class' => [static::AJAX_INLINE_ADD_FORM_CLASS]],
              '#weight'     => -98,
            ];
            break;
        }
      }

      if (!empty($profiles = &$profile_type['profiles'])) {
        foreach (Element::children($profiles) as $pid) {
          $profile = &$profiles[$pid];
          $profile['#attributes']['class'][] = static::AJAX_INLINE_ENTITY_CLASS . $pid;
          /** @var \Drupal\profile\Entity\Profile $entity */
          if ($entity = $profile['profile']['#profile'] ?? NULL) {
            if ($entity->isDefault()) {
              if (isset($settings['hide_default'])) {
                $profile['#access'] = !$settings['hide_default'];
              }
              if (isset($profile['operations']['delete']) && isset($settings['delete_default'])) {
                $profile['operations']['delete']['#access'] = $settings['delete_default'];
              }
            }
            else {
              if (isset($profile['operations']['set_default']) && isset($settings['change_default'])) {
                $profile['operations']['set_default']['#access'] = $settings['change_default'];
                if ($title = $settings['change_default_text'] ?? NULL) {
                  $profile['operations']['set_default']['#title'] = $title;
                }
              }
            }

            if ($handling !== 'default') {
              $operations = &$profile['operations'];
              foreach (Element::children($operations) as $child) {
                $operation = &$operations[$child];
                if ($child !== 'set_default') {
                  if ($handling === 'inline' && $child === 'edit') {
                    /** @var Url $url */
                    $url = $operation['#url'];
                    $operation['#url'] = Url::fromRoute(
                      'codev_commerce_address_book.address_book.edit_form_ajax',
                      $url->getRouteParameters());
                    $operation['#attributes']['class'][] = 'use-ajax';
                  }
                  else {
                    Utils::appendDialogModalAttr($operation['#attributes']);
                  }
                }
                else {
                  $operation['#attributes']['class'][] = 'use-ajax';
                }
              }
            }
          }
        }
      }
    }

    $context = ['user' => $user];
    $hook = 'address_book_overview_pre_render';
    $this->themeHandler->alter($hook, $build, $context);
    $this->moduleHandler->alter($hook, $build, $context);

    return $build;
  }

  /**
   * Return an ajax replace callback.
   *
   * @param \Drupal\user\UserInterface                  $user
   *   The user accounts.
   * @param \Drupal\profile\Entity\ProfileTypeInterface $profile_type
   *   The profile types.
   *
   * @return AjaxResponse
   *   The response.
   *
   * @noinspection PhpUnused
   */
  public function addFormAjax(UserInterface $user, ProfileTypeInterface $profile_type): AjaxResponse {
    $response = new AjaxResponse();
    $form = parent::addForm($user, $profile_type);
    $form['#prefix'] = sprintf('<div id="%s" class="%s">', $form['#wrapper_id'], static::AJAX_INLINE_ADD_FORM_CLASS);
    $selector = '.' . static::AJAX_INLINE_ADD_FORM_CLASS;
    $response->addCommand(new ReplaceCommand($selector, $form));
    $response->addCommand(new FocusFirstCommand($selector));
    return $response;
  }

  /**
   * Return an ajax replace callback.
   *
   * @param \Drupal\user\UserInterface     $user
   *   The user accounts.
   * @param \Drupal\profile\Entity\Profile $profile
   *
   * @return AjaxResponse
   *   The response.
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function editFormAjax(UserInterface $user, Profile $profile): AjaxResponse {
    $response = new AjaxResponse();
    $form = $this->entityFormBuilder->getForm($profile, 'address-book-edit');
    $form['#prefix'] = sprintf('<div id="%s" class="%s %s">',
      $form['#wrapper_id'],
      static::AJAX_INLINE_EDIT_FORM_CLASS,
      static::AJAX_INLINE_EDIT_FORM_CLASS . '-' . $profile->id()
    );
    $selector = '.' . static::AJAX_INLINE_ENTITY_CLASS . $profile->id();
    $response->addCommand(new ReplaceCommand($selector, $form));
    $response->addCommand(new FocusFirstCommand($selector));
    return $response;
  }

  /**
   * {@inheritDoc}
   *
   * @noinspection PhpUnused
   */
  public function setDefault(ProfileInterface $profile) {
    $ret = parent::setDefault($profile);
    if (Drupal::request()->isXmlHttpRequest()) {
      $response = new AjaxResponse();
      $build = $this->overviewPage($profile->getOwner());
      $response->addCommand(new ReplaceCommand('.' . static::AJAX_WRAPPER_CLASS, $build));
      return $response;
    }
    return $ret;
  }

}
