<?php

namespace Drupal\codev_commerce_address_book;

use Drupal\codev_utils\SettingsBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Settings.php
 * .
 */

/**
 * Class Settings.
 *
 * @package Drupal\codev_commerce_address_book
 *
 * @noinspection PhpUnused
 */
class Settings extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  public const MODULE_NAME = 'codev_commerce_address_book';

}
