<?php

namespace Drupal\codev_commerce_address_book\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AdminSettingsForm.
 *
 * @noinspection PhpUnused
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'codev_commerce_address_book.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config($this->getEditableConfigNames()[0]);
    $date = $config->getRawData();

    $form['handling'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Address book handling type. (Default, Modal, Ajax ...)'),
      '#options'       => [
        'default' => $this->t('Default'),
        'modal'   => $this->t('Modal'),
        'inline'  => $this->t('Inline'),
      ],
      '#default_value' => $date['handling'] ?? 'default',
      '#required'      => TRUE,
    ];

    $form['default'] = [
      '#type'  => 'details',
      '#title' => $this->t('Default profile settings'),
      '#open'  => TRUE,
    ];

    $form['default']['hide_default'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Hide default profile'),
      '#description'   => $this->t('Hide default profile in address book.'),
      '#default_value' => $date['hide_default'] ?? FALSE,
    ];

    $form['default']['change_default'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable change button'),
      '#description'   => $this->t('Allow to change the default profile.'),
      '#default_value' => $date['change_default'] ?? TRUE,
      '#states'        => [
        'visible' => [
          ':input[name="hide_default"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['default']['change_default_text'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Button text'),
      '#default_value' => $date['change_default_text'] ?? $this->t('Set as default'),
      '#states'        => [
        'visible' => [
          ':input[name="change_default"]' => ['checked' => TRUE],
          ':input[name="hide_default"]'   => ['checked' => FALSE],
        ],
      ],
    ];

    $form['default']['delete_default'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable delete button'),
      '#description'   => $this->t('Allow to delete the default profile.'),
      '#default_value' => $date['delete_default'] ?? TRUE,
      '#states'        => [
        'visible' => [
          ':input[name="hide_default"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();

    $this->config($this->getEditableConfigNames()[0])
      ->set('handling', $values['handling'])
      ->set('hide_default', $values['hide_default'])
      ->set('change_default', $values['change_default'])
      ->set('change_default_text', $values['change_default_text'])
      ->set('delete_default', $values['delete_default'])
      ->save();
  }

}
