<?php

namespace Drupal\codev_commerce_address_book\Routing;

use Drupal\codev_commerce_address_book\Controller\AddressBookController;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 *
 * @noinspection PhpUnused
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('commerce_order.address_book.overview')) {
      $route->setDefault('_controller', AddressBookController::class . '::overviewPage');
    }

    if ($route = $collection->get('commerce_order.address_book.set_default')) {
      $route->setDefault('_controller', AddressBookController::class . '::setDefault');
    }
  }

}
