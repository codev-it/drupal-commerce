<?php

/**
 * @file
 * Hooks provided by the Codev commerce address book module.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Render\Element;

/**
 * Perform alterations before the address book overview page is rendered.
 *
 * @param array $variables
 * @param array $context
 *
 * @ingroup codev_commerce_address_book
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function hook_address_book_overview_pre_render_alter(array &$variables, array $context) {
  foreach (Element::children($variables) as $type) {
    $profile_type = &$build[$type];
    if (!empty($profiles = &$profile_type['profiles'])) {
      foreach (Element::children($profiles) as $pid) {
        $profile = &$profiles[$pid];
        $profile['#attributes']['class'][] = 'class';
      }
    }
  }
}

/**
 * @} End of "addtogroup hooks".
 */
