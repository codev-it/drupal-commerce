<?php

use Drupal\codev_commerce_address_book\Controller\AddressBookController;
use Drupal\codev_commerce_address_book\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_commerce_product_form_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_commerce_address_book_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  switch ($form_id) {
    case 'profile_customer_address-book-add_form':
    case 'profile_customer_address-book-edit_form':
    case 'profile_customer_address-book-delete_form':
      $settings = Settings::toArray();
      $handling = $settings['handling'] ?? '';

      if (isset($form['actions']['delete'])) {
        $form['actions']['delete']['#access'] = $settings['delete_default'] ?? TRUE;
      }

      if (Drupal::request()->isXmlHttpRequest()) {
        $form['actions']['submit']['#submit'][] = '_codev_utils_disable_redirect_form_submit';

        if (isset($form['actions']['delete'])) {
          Utils::appendDialogModalAttr($form['actions']['delete']['#attributes']);
        }

        if (isset($form['actions']['cancel'])) {
          $form['actions']['cancel'] = [
            '#type'   => 'submit',
            '#value'  => $form['actions']['cancel']['#title'] ?? '',
            '#ajax'   => [
              'callback' => '_codev_commerce_address_book_close_modal_callback',
              'wrapper'  => $form['#id'],
            ],
            '#submit' => ['_codev_utils_disable_redirect_form_submit'],
            '#weight' => 99,
          ];
        }

        $form['actions']['submit']['#ajax'] = [
          'disable-refocus' => TRUE,
          'callback'        => '_codev_commerce_address_book_refresh_callback',
          'wrapper'         => $form['#id'],
        ];

        if ($handling === 'modal' || $form_id === 'profile_customer_address-book-delete_form') {
          $form['actions']['submit']['#ajax']['callback'] = '_codev_commerce_address_book_close_modal_and_refresh_callback';
        }
      }
      break;
  }
}

/**
 * Close modal after success submit.
 *
 * @param                                      $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *
 * @return AjaxResponse
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_commerce_address_book_close_modal_callback(&$form, FormStateInterface $form_state): AjaxResponse {
  $response = new AjaxResponse();
  $response->addCommand(new CloseDialogCommand());
  return $response;
}

/**
 * Refresh address book page.
 *
 * @param                                      $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *
 * @return array|AjaxResponse
 *
 * @noinspection PhpUnused
 */
function _codev_commerce_address_book_refresh_callback($form, FormStateInterface $form_state) {
  if ($form_state->isSubmitted() && !$form_state->hasAnyErrors()) {
    $response = new AjaxResponse();
    /** @var \Drupal\commerce_order\Form\ProfileAddressBookForm $form_object */
    $form_object = $form_state->getFormObject();
    /** @var \Drupal\profile\Entity\Profile $entity */
    $entity = $form_object->getEntity();
    $controller = AddressBookController::create(Drupal::getContainer());
    $build = $controller->overviewPage($entity->getOwner());
    switch (Settings::get('handling')) {
      case 'handling':
        $build = NestedArray::getValue($build, [
            $entity->bundle(),
            'profiles',
            $entity->id(),
          ]) ?? $build;
        $build['status_messages'] = [
          '#type'   => 'status_messages',
          '#weight' => -99,
        ];
        if ($form_object->getFormId() === 'profile_customer_address-book-add_form') {
          $response->addCommand(new PrependCommand('.' . AddressBookController::AJAX_INLINE_ENTITY_WRAPPER_CLASS, $build));
          $response->addCommand(new ReplaceCommand('.' . AddressBookController::AJAX_INLINE_ADD_FORM_CLASS, [
            '#type'       => 'container',
            '#attributes' => ['class' => [AddressBookController::AJAX_INLINE_ADD_FORM_CLASS]],
          ]));
        }
        else {
          $response->addCommand(new ReplaceCommand('.' . AddressBookController::AJAX_INLINE_EDIT_FORM_CLASS . '-' . $entity->id(), $build));
        }
        break;
      default:
        $response->addCommand(new ReplaceCommand('.' . AddressBookController::AJAX_WRAPPER_CLASS, $build));
    }
    return $response;
  }
  return $form;
}

/**
 * Close modal after success submit and refresh address book page.
 *
 * @param                                      $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *
 * @return array|AjaxResponse
 *
 * @noinspection PhpUnused
 */
function _codev_commerce_address_book_close_modal_and_refresh_callback($form, FormStateInterface $form_state) {
  $ret = _codev_commerce_address_book_refresh_callback($form, $form_state);
  if ($ret instanceof AjaxResponse) {
    $ret->addCommand(new CloseDialogCommand());
  }
  return $ret;
}

