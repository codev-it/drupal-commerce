<?php

namespace Drupal\codev_commerce_tax\Plugin\Commerce\TaxType;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_tax\Annotation\CommerceTaxType;
use Drupal\commerce_tax\Plugin\Commerce\TaxType\EuropeanUnionVat;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides the European Union VAT intra-community supply tax type.
 *
 * @CommerceTaxType(
 *   id = "european_union_vat_store",
 *   label = "European Union VAT (Use Store Address)",
 * )
 *
 * @noinspection PhpUnused
 */
class EuropeanUnionVatStore extends EuropeanUnionVat {

  /**
   * {@inheritDoc}
   */
  protected function resolveZones(OrderItemInterface $order_item, ProfileInterface $customer_profile): array {
    $zones = parent::resolveZones($order_item, $customer_profile);
    if (!empty($zones)) {
      /** @var \Drupal\commerce_tax\TaxZone $zone */
      $zone = end($zones);
      if ($zone->getId() !== 'ic') {
        $store = $order_item->getOrder()->getStore();
        return $this->getMatchingZones($store->getAddress()) ?? [];
      }
    }
    return $zones;
  }

}
