<?php


namespace Drupal\codev_commerce;

use Drupal\codev_utils\SettingsBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Settings.php
 * .
 */

/**
 * Class Settings.
 *
 * @package      Drupal\codev_commerce
 *
 * @noinspection PhpUnused
 */
class Settings extends SettingsBase {

  /**
   * Eu countries.
   */
  public const EU_COUNTRIES = [
    'AT',
    'BE',
    'BG',
    'CY',
    'CZ',
    'DE',
    'DK',
    'EE',
    'ES',
    'FI',
    'FR',
    'GB',
    'GR',
    'HR',
    'HU',
    'IE',
    'IT',
    'LT',
    'LU',
    'LV',
    'MT',
    'NL',
    'PL',
    'PT',
    'RO',
    'SE',
    'SI',
    'SK',
  ];

}
