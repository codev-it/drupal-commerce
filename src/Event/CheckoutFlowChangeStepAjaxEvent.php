<?php

namespace Drupal\codev_commerce\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;

/**
 * Event that is fired when checkout flow is changing the step.
 */
class CheckoutFlowChangeStepAjaxEvent extends Event {

  /**
   * Event name.
   */
  const EVENT_NAME = 'checkout_flow_change_step_ajax';

  /**
   * The ajax response.
   *
   * @var AjaxResponse
   */
  public AjaxResponse $response;

  /**
   * The form array.
   *
   * @var array
   */
  protected array $form;

  /**
   * The form state object.
   *
   * @var FormStateInterface
   */
  protected FormStateInterface $formState;

  /**
   * Constructs the object.
   *
   */
  public function __construct(AjaxResponse $response, array $form, FormStateInterface $form_state) {
    $this->response = $response;
    $this->form = $form;
    $this->formState = $form_state;
  }

  /**
   * Return current step id.
   *
   * @return string|null
   */
  public function getStepId(): ?string {
    return $this->form['#step_id'] ?? NULL;
  }

  /**
   * Return form array.
   *
   * @return array
   */
  public function getForm(): array {
    return $this->form;
  }

  /**
   * Return form state object.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

}
