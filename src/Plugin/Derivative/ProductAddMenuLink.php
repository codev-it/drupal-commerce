<?php

namespace Drupal\codev_commerce\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links for the Products.
 *
 * @noinspection PhpUnused
 */
class ProductAddMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Creates a ProductAddMenuLink instance.
   *
   * @param                            $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $links = [];

    try {
      $product_types = $this->entityTypeManager
        ->getStorage('commerce_product_type')
        ->loadMultiple();
      foreach ($product_types as $product_type) {
        $product_type_id = $product_type->id();
        $links[$product_type_id] = [
            'title'            => $product_type->label(),
            'route_name'       => 'entity.commerce_product.add_form',
            'route_parameters' => ['commerce_product_type' => $product_type_id],
          ] + $base_plugin_definition;
      }
    } catch (Exception $exception) {

    }

    return $links;
  }

}
