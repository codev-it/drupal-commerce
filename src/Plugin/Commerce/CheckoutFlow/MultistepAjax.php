<?php

namespace Drupal\codev_commerce\Plugin\Commerce\CheckoutFlow;

use Drupal;
use Drupal\codev_commerce\Event\CheckoutFlowChangeStepAjaxEvent;
use Drupal\codev_utils\Ajax\HistoryUpdateCommand;
use Drupal\codev_utils\Ajax\ReplacePageTitleCommand;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_checkout\Annotation\CommerceCheckoutFlow;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\MultistepDefault;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the ajax multistep checkout flow.
 *
 * @CommerceCheckoutFlow(
 *   id = "multistep_ajax",
 *   label = "Multistep - AJAX",
 * )
 *
 * @noinspection PhpUnused
 */
class MultistepAjax extends MultistepDefault {

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected CheckoutOrderManagerInterface $checkoutOrderManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pane_id, $pane_definition) {
    $instance = parent::create($container, $configuration, $pane_id, $pane_definition);
    $instance->checkoutOrderManager = $container->get('commerce_checkout.checkout_order_manager');
    $instance->formBuilder = $container->get('form_builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'skip_payment_process' => FALSE,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['panes']['#weight'] = 9999;

    $form['skip_payment_process'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Disable AJAX on payment process step.'),
      '#description'   => t('By problems with the payment process disable AJAX from the payment process.'),
      '#default_value' => $this->configuration['skip_payment_process'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['skip_payment_process'] = boolval($form_state->getValue([
      'configuration',
      'skip_payment_process',
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $step_id = NULL): array {
    $form = parent::buildForm($form, $form_state, $step_id);
    $form['#attached']['library'][] = 'core/drupal.ajax';
    $form['#attached']['library'][] = 'codev_utils/ajax.history-update-command';
    $form['#attached']['library'][] = 'codev_utils/ajax.replace-page-title-command';
    $form['#attached']['library'][] = 'codev_commerce/ajax.checkout-change-command';

    $form['#attributes']['data-checkout-step'] = $step_id;
    if (Drupal::request()->isXmlHttpRequest()) {
      $form['status_message'] = [
        '#type'   => 'status_messages',
        '#weight' => -99,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $form_id = $form['#step_id'];
    $ajax = [
      'disable-refocus' => TRUE,
      'wrapper'         => $form['#id'],
      'callback'        => '::ajaxStepChangeCallback',
    ];

    if (isset($actions['next'])) {
      $next_step_id = $this->getNextStepId($form_id);
      $skip_payment = $this->configuration['skip_payment_process'];
      $next_ajax_disable = $skip_payment && $next_step_id === 'payment';
      if (!$next_ajax_disable) {
        $actions['next']['#submit'][] = '::ajaxNextStepSubmit';
        $actions['next']['#ajax'] = $ajax;
      }

      if (isset($actions['next']['#suffix'])) {
        unset($actions['next']['#suffix']);
        $steps = $this->getSteps();
        $previous_step_id = $this->getPreviousStepId($form_id);
        $prev_ajax_disable = $skip_payment && $previous_step_id === 'payment';
        if (!$prev_ajax_disable) {
          $actions['prev'] = [
            '#type'   => 'submit',
            '#value'  => $steps[$previous_step_id]['previous_label'] ?? $this->t('Back'),
            '#submit' => ['::ajaxPreviewStepSubmit'],
            '#weight' => -999,
            '#ajax'   => $ajax,
            '#op'     => 'prev',
          ];
        }
      }
    }
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $triggering_element = $form_state->getTriggeringElement();
    if (isset($triggering_element['#op']) && $triggering_element['#op'] == 'prev') {
      $form_state->clearErrors();
    }
  }

  /**
   * Ajax next submit handler
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   */
  public function ajaxNextStepSubmit(array $form, FormStateInterface $form_state) {
    $this->ajaxChangeStepSubmit($this->getNextStepId($form['#step_id']), $form_state);
  }

  /**
   * Ajax preview submit handler
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @noinspection PhpUnused
   */
  public function ajaxPreviewStepSubmit(array $form, FormStateInterface $form_state) {
    $this->ajaxChangeStepSubmit($this->getPreviousStepId($form['#step_id']), $form_state);
  }

  /**
   * Ajax next callback handler
   *
   * @param string                               $step_id
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  private function ajaxChangeStepSubmit(string $step_id, FormStateInterface $form_state) {
    $form_state->addBuildInfo('args', [$step_id]);
    $form_state->disableRedirect();
    $form_state->setRebuild();
  }

  /**
   * Ajax change step form callback.
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *
   * @noinspection PhpUnused
   */
  public function ajaxStepChangeCallback(array $form, FormStateInterface $form_state): AjaxResponse {
    /** @var \Drupal\codev_commerce\Plugin\Commerce\CheckoutFlow\MultistepAjax $callback_object */
    $callback_object = $form_state->getBuildInfo()['callback_object'] ?? NULL;
    $step_id = $form['#step_id'];
    $title = $this->getSteps()[$step_id]['label'] ?? $this->t('Checkout');
    $form_id = '#' . Html::cleanCssIdentifier($form['#form_id']);
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand($form_id, $form));
    $response->addCommand(new ReplacePageTitleCommand($title));
    if ($order_id = $this->_orderId ?? $callback_object->_orderId ?? NULL) {
      $response->addCommand(new HistoryUpdateCommand(
        Url::fromRoute('commerce_checkout.form', [
          'commerce_order' => $order_id,
          'step'           => $step_id,
        ]),
        $title, ['selector' => $form_id]
      ));
    }

    // Ajax change event handler.
    $event = new CheckoutFlowChangeStepAjaxEvent($response, $form, $form_state);
    $this->eventDispatcher->dispatch($event, CheckoutFlowChangeStepAjaxEvent::EVENT_NAME);
    return $event->response;
  }

  /**
   * {@inheritdoc}
   */
  public function redirectToStep($step_id) {
    if (Drupal::request()->isXmlHttpRequest()) {
      try {
        parent::redirectToStep($step_id);
      } catch (NeedsRedirectException $e) {
        throw new NeedsRedirectException(Url::fromRoute(
          'commerce_checkout.form', [
          'commerce_order' => $this->getOrder()->id(),
          'step'           => $step_id,
        ], [
          'query' => [
            MainContentViewSubscriber::WRAPPER_FORMAT => 'drupal_ajax',
          ],
        ])->toString());
      }
    }
    else {
      parent::redirectToStep($step_id);
    }
  }

}
