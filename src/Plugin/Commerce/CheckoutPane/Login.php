<?php

namespace Drupal\codev_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\codev_commerce\Plugin\Commerce\CheckoutFlow\MultistepAjax;
use Drupal\commerce_checkout\Annotation\CommerceCheckoutPane;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\Login as LoginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the login pane.
 *
 * @CommerceCheckoutPane(
 *   id = "login",
 *   label = @Translation("Login or continue as guest"),
 *   default_step = "login",
 * )
 */
class Login extends LoginBase {

  /**
   * {@inheritDoc
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);
    $callback_object = $form_state->getBuildInfo()['callback_object'] ?? NULL;
    if ($callback_object instanceof MultistepAjax && isset($pane_form['guest']['continue'])) {
      $pane_form['guest']['continue']['#ajax'] = [
        'disable-refocus' => TRUE,
        'wrapper'         => $complete_form['#id'],
        'callback'        => '::ajaxStepChangeCallback',
      ];
    }
    return $pane_form;
  }

  /**
   * {@inheritDoc
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    parent::submitPaneForm($pane_form, $form_state, $complete_form);
    $triggering_element = $form_state->getTriggeringElement();
    if (isset($triggering_element['#op']) && $triggering_element['#op'] == 'continue') {
      $callback_object = $form_state->getBuildInfo()['callback_object'] ?? NULL;
      $form_state->addBuildInfo('args', [$callback_object->getNextStepId($complete_form['#step_id'])]);
      $form_state->disableRedirect();
      $form_state->setRebuild();
    }
  }

}
