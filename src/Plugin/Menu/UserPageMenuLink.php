<?php

namespace Drupal\codev_commerce\Plugin\Menu;

use Drupal;
use Drupal\Core\Menu\MenuLinkDefault;

/**
 * User .
 *
 * @noinspection PhpUnused
 */
class UserPageMenuLink extends MenuLinkDefault {

  /**
   * {@inheritDoc}
   */
  public function getRouteParameters(): array {
    return [
      'user' => Drupal::currentUser()->id(),
    ];
  }

}
